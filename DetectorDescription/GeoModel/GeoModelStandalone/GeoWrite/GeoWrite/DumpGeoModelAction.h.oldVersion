/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef DumpGeoModelAction_H
#define DumpGeoModelAction_H

// local includes


#include "GeoModelDBManager/GMDBManager.h"

// GeoModel includes
#include "GeoModelKernel/GeoNodeAction.h"
#include "GeoModelKernel/GeoGraphNode.h"
#include "GeoModelKernel/GeoShape.h"
#include "GeoModelKernel/GeoMaterial.h"
#include "GeoModelKernel/GeoLogVol.h"
#include "GeoModelKernel/GeoXF.h"
#include "GeoModelKernel/GeoAlignableTransform.h"

// Qt includes
//#include <QSqlDatabase>
#include <QStringList>
#include <QVariant>
#include <QString>
#include <QMap>

/**
 * \class DumpGeoModelAction
 *
 * DumpGeoModelAction acts on objects of the GeoModel tree
 * persitifying them offline.
 */
class DumpGeoModelAction  : public GeoNodeAction
{
public:
	/**
	 * @brief Constructor
	 *
	 * Constructor sets up connection with db and opens it
	 * @param path - absolute path to db file
	 */
	DumpGeoModelAction(GMDBManager& db);

	/**
	 * @brief Destructor
	 */
	~DumpGeoModelAction();

	virtual void handlePhysVol (const GeoPhysVol *vol); //	Handles a physical volume.
	virtual void handleFullPhysVol (const GeoFullPhysVol *vol);
	virtual void handleSerialDenominator (const GeoSerialDenominator *sD); //	Handles a Serial Denominator.
	virtual void handleSerialTransformer (const GeoSerialTransformer *obj);
	virtual void handleTransform (const GeoTransform *);
	virtual void handleNameTag (const GeoNameTag *);



private:

	// define copy constructor, needed for the GeoModelAction subclass
	DumpGeoModelAction(const DumpGeoModelAction &right);

	// define assignment operator, needed for the GeoModelAction subclass
	DumpGeoModelAction & operator=(const DumpGeoModelAction &right);

	void handleVPhysVolObjects(const GeoVPhysVol* vol);
	void handleReferencedPhysVol (const GeoVPhysVol *vol); // Handles a physical volume referenced by a SerialTrasformer

	void showMemoryMap();

	//	bool isObjectStored(const GeoLogVol* pointer);
	//	bool isObjectStored(const GeoMaterial* pointer);
	//	bool isObjectStored(const GeoGraphNode* pointer);
	//	bool isObjectStored(const GeoShape* pointer);

	QVariant storeShape(const GeoShape* shape);
	QVariant storeTranform(const GeoTransform* node);

	QVariant storeObj(const GeoMaterial* pointer, const QString name);
	QVariant storeObj(const GeoShape* pointer, const QString type, const QString parameters);
	QVariant storeObj(const GeoLogVol* pointer, const QString name, const QVariant shapeId, const QVariant materialId);
	QVariant storeObj(const GeoPhysVol* pointer, const QVariant logvolId, const QVariant parentId = QVariant(), bool isRootVolume = false );
	QVariant storeObj(const GeoFullPhysVol* pointer, const QVariant logvolId, const QVariant parentId = QVariant(), bool isRootVolume = false );
	QVariant storeObj(const GeoSerialDenominator* pointer, const QString baseName);
	QVariant storeObj(const GeoSerialTransformer* pointer, const QVariant functionId, const QVariant volId, const QString volType, const unsigned int copies);
	QVariant storeObj(const GeoXF::Function* pointer, const QString expression);
	QVariant storeObj(const GeoTransform* pointer, const std::vector<double> parameters);
	QVariant storeObj(const GeoAlignableTransform* pointer, const std::vector<double> parameters);
	QVariant storeObj(const GeoNameTag* pointer, const QString name);

	int getChildPosition(unsigned int parentId);
	void storeChildPosition(const QVariant parentId, const QString parentType, const QVariant childVol, const unsigned int childPos, const QString childType);

	//	int storeObj(const GeoGraphNode* pointer, const QVariant logvolId, const QVariant parentId = QVariant() );

	bool isAddressStored(const QString address);
	void storeAddress(const QString address, QVariant id);

	QVariant getStoredIdFromAddress(QString address);

	//	QVariant getStoredIdFromPointer(const GeoMaterial* pointer);
	QString getAddressStringFromPointer(const GeoMaterial* pointer);
	QString getAddressStringFromPointer(const GeoShape* pointer);
	QString getAddressStringFromPointer(const GeoLogVol* pointer);
	QString getAddressStringFromPointer(const GeoPhysVol* pointer);
	QString getAddressStringFromPointer(const GeoVPhysVol* pointer);
	QString getAddressStringFromPointer(const GeoSerialDenominator* pointer);
	QString getAddressStringFromPointer(const GeoSerialTransformer* pointer);
	QString getAddressStringFromPointer(const GeoXF::Function* pointer);
	QString getAddressStringFromPointer(const GeoTransform* pointer);
	QString getAddressStringFromPointer(const GeoNameTag* pointer);

	QString getQStringFromOss(std::ostringstream &oss);

	std::vector<double> getTransformParameters(HepGeom::Transform3D); // TODO: to be moved to an Utility class, so we can use it from TransFunctionRecorder as well.
	QString getShapeParameters(const GeoShape*);

	QString getGeoTypeFromVPhysVol(const GeoVPhysVol* vol);


	GMDBManager* m_dbManager;

	QMap<QString, QVariant> m_memMap; // TODO: maybe move to QHash??
	QMap<QString, QString> m_memMap_Tables; // TODO: maybe move to QHash??
	QMap<unsigned int, unsigned int> m_parentChildrenMap;

	// keep track of the number of visited tree nodes
	unsigned int m_len;
	unsigned int m_len_nChild;

	bool m_rootVolumeFound;

};

#endif // DumpGeoModelAction_H
